﻿using BaldejFramework.Assets;
using BaldejFramework.Render;
using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;

namespace BaldejFramework.Components
{
    public class Mesh : Component, IDisposable
    {
        #region Public Variables
        public string componentID => "Mesh";
        public GameObject? owner { get; set; }

        public MeshAsset Asset
        {
            get => _asset;
            set
            {
                _asset = value;
                Update();
            }
        }
        public int CurrentAnimationFrame
        {
            get => _currentAnimationFrame;
            set
            {
                _currentAnimationFrame = value;
                Update();
            }
        }

        public int AnimationEndFrame;
        public int AnimationStartFrame;

        public bool LoopAnimation = false;
        public Shader shader { get; set; }
        public TextureAsset? Texture;
        public Action? OnAnimationEnd;
        #endregion

        #region Private Variables
        private MeshAsset _asset;
        private int _currentAnimationFrame = 0;
        private int _vbo = GL.GenBuffer();
        private int _uvbo = GL.GenBuffer();
        private int _nbo = GL.GenBuffer();
        private int _vao = GL.GenVertexArray();

        private float[] _verts { get => _asset.Frames[CurrentAnimationFrame].Vertices; }
        private float[] _uvs { get => _asset.Frames[CurrentAnimationFrame].UVs; }
        private float[] _normals { get => _asset.Frames[CurrentAnimationFrame].Normals; }
        #endregion

        public Mesh(MeshAsset meshAsset, TextureAsset? texture, string vertShaderPath = @"Shaders\vert.shader", string fragShaderPath = @"Shaders\frag.shader")
        {
            shader = new Shader(vertShaderPath, fragShaderPath);
            _asset = meshAsset;
            Texture = texture;

            Update();
        }

        public void OnRender()
        {
            GL.BindVertexArray(_vao);

            if (Texture != null)
            {
                Texture?.Use();
                shader.SetInt("texture0", 2);
            }
            shader.SetVector3("lightColor", Render.Render.SunColor);
            shader.SetVector3("lightPos", Render.Render.SunPosition);
            // shader.SetVector3("viewPos", Render.Render.camera.Position);
            shader.Use();

            GL.DrawArrays(PrimitiveType.Triangles, 0, _verts.Length / 3);
        }

        public void OnUpdate()
        {
            if (CurrentAnimationFrame + 1 <= AnimationEndFrame)
                CurrentAnimationFrame++;
            else if (CurrentAnimationFrame + 1 > AnimationEndFrame && LoopAnimation)
                CurrentAnimationFrame = AnimationStartFrame;
            else if (CurrentAnimationFrame + 1 > AnimationEndFrame && !LoopAnimation)
                OnAnimationEnd?.Invoke();

            Transform transformComponent = (Transform)owner.GetComponent("Transform");
            Matrix4 transform = Matrix4.Identity;
            transform = transform * Matrix4.CreateRotationX(transformComponent.Rotation.X);
            transform = transform * Matrix4.CreateRotationY(transformComponent.Rotation.Y);
            transform = transform * Matrix4.CreateRotationZ(transformComponent.Rotation.Z);
            transform = transform * Matrix4.CreateScale(transformComponent.Scale.X, transformComponent.Scale.Y, transformComponent.Scale.Z);
            transform = transform * Matrix4.CreateTranslation(transformComponent.Position.X, transformComponent.Position.Y, transformComponent.Position.Z);

            shader.SetMatrix4("model", transform);
            shader.SetMatrix4("view", Render.Render.camera.GetViewMatrix());
            shader.SetMatrix4("projection", Render.Render.camera.GetProjectionMatrix());
        }

        private void Update()
        {
            GL.BindVertexArray(_vao);
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, _verts.Length * sizeof(float), _verts, BufferUsageHint.DynamicDraw);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 3 * sizeof(float), 0);
            GL.EnableVertexAttribArray(0);

            GL.BindBuffer(BufferTarget.ArrayBuffer, _uvbo);
            GL.BufferData(BufferTarget.ArrayBuffer, _uvs.Length * sizeof(float), _uvs, BufferUsageHint.DynamicDraw);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 2 * sizeof(float), 0);
            GL.EnableVertexAttribArray(1);

            GL.BindBuffer(BufferTarget.ArrayBuffer, _nbo);
            GL.BufferData(BufferTarget.ArrayBuffer, _normals.Length * sizeof(float), _normals, BufferUsageHint.DynamicDraw);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, false, 3 * sizeof(float), 0);
            GL.EnableVertexAttribArray(2);
        }

        public void Dispose()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindVertexArray(0);
            GL.DeleteBuffer(_vbo);
            GL.DeleteBuffer(_nbo);
            GL.DeleteBuffer(_uvbo);
            GL.DeleteVertexArray(_vao);
            GL.DeleteShader(shader.Handle);
        }
    }
}