﻿using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using BaldejFramework.UI;

namespace BaldejFramework.Render
{
    public static class Render
    {
        public static GameWindow window;
        public static Camera camera;
        public static float[] SkyColor = { 0.5f, 0.9f, 1f };
        public static Vector3 SunColor = new Vector3(1, 1, 1);
        public static Vector3 SunPosition = new Vector3(-5, 0, 0);

        public static float DeltaTime;

        public static object game;
        public static Type gameType;

        public static int CurrentTextureUnit;
        public static int CurrentClipPlane;

        public static bool IsPaused = false;

        public static void RunRender()
        {
            // creating window
            NativeWindowSettings nws = new NativeWindowSettings();
            nws.Title = "baldejgame";
            nws.Size = new Vector2i(500, 500);
            nws.Profile = ContextProfile.Core;
            nws.Flags = ContextFlags.ForwardCompatible;
            window = new GameWindow(GameWindowSettings.Default, nws);

            // first we run Load func
            window.Load += Window_Load;

            // small lambda func for Resize
            window.Resize += (ResizeEventArgs obj) =>
            {
                GL.Viewport(0, 0, obj.Width, obj.Height);

                Console.WriteLine("window resize");

                camera.AspectRatio = window.Size.X / (float)window.Size.Y;

                UiManager.Resize();
            };

            // then Update, then we starting Render
            window.UpdateFrame += Window_UpdateFrame;
            window.RenderFrame += Window_RenderFrame;
            window.Closing += Window_Closing;
            window.VSync = VSyncMode.On;
            // let's start our window!
            window.Run();
        }

        private static void Window_Closing(System.ComponentModel.CancelEventArgs obj)
        {
            Console.WriteLine("Closing game, bye!");
            System.Environment.Exit(1);
        }

        private static void Window_Load()
        {
            Console.WriteLine("Hello, game window loaded!");
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.DepthTest);

            camera = new Camera(Vector3.UnitZ * 3, window.Size.X / (float)window.Size.Y);
            Physics.UISpace.ForceUpdater.Gravity = new BEPUutilities.Vector3(0, 0, 0);
            gameType.GetMethod("Start").Invoke(game, null);
            Console.WriteLine("GL version: " + window.APIVersion);
        }

        private static void Window_UpdateFrame(FrameEventArgs obj)
        {
            if (!IsPaused)
            {
                SceneManager.Update();
                Physics.Update();
                gameType.GetMethod("Update").Invoke(game, Array.Empty<object>());
            }
            gameType.GetMethod("UnpausedUpdate").Invoke(game, Array.Empty<object>());
        }

        private static void Window_RenderFrame(FrameEventArgs obj)
        {
            if (!IsPaused)
            {
                CurrentTextureUnit = 0;

                if (!UiManager.IsInDebugMode)
                {
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
                }
                else
                {
                    GL.Disable(EnableCap.Blend);
                }

                GL.ClearColor(SkyColor[0], SkyColor[1], SkyColor[2], 1);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                SceneManager.Render();

                UiManager.Render();

                window.SwapBuffers();
            }
        }
    }
}
